#!/bin/bash

###############################################################################
#                                 Library                                     # 
###############################################################################

###############################################################################
# Functions that may be used at any point in the programs execution by any file 
# including text formatting and assertions
###############################################################################



###############################################################################
#                               Text format                                   #
###############################################################################
# Getting directory information:
CURRENT_SCRIPT="${BASH_SOURCE}"
TARGET="$(readlink "$CURRENT_SCRIPT}")"
CURRENT_DIR="$(dirname "${CURRENT_SCRIPT}")/${TARGET}"
COLORS_FILE="${CURRENT_DIR}colors.sh"

# Importing format variables:
source ${COLORS_FILE}

###############################################################################
# Formats a text according to a desired color or style.
# Globals:
#   None
# Arguments:
#   [style] [text]
#   *the style must be in colors.sh
# Retuns:
#	  None
###############################################################################
f_text(){
  echo -e "${1}${2}${NC}\0"
  return 0
}

###############################################################################
# The next format functions use f_text to format a text passing the proper.
# style from color.sh according to the desired case.
# Globals:
#	  None
# Arguments:
#   text
# Retuns:
#   None
###############################################################################
green_text(){
  f_text $Green   "$1" 
}
yellow_text(){
  f_text $Yellow  "$1" 
}
red_text(){
  f_text $Red     "$1" 
}
bgreen_text(){
  f_text $BGreen  "$1" 
}
byellow_text(){
  f_text $BYellow "$1" 
}
bred_text(){
  f_text $BRed    "$1"
}


###############################################################################
#                               Assertions                                    # 
###############################################################################

###############################################################################
# Makes an assertion according to the state of success of the last command
# executed
# Globals:
#   Exit status of last task - $?
# Arguments:
#   None
# Retuns:
#   None
###############################################################################
command_assertion(){
  if  [ $? -eq 0 ]; then
    bgreen_text "[DONE] Exit status: $?"
	else
    bred_text "[ERROR] Faililed at performing this task"
    bred_text "[ERROR] Exit status: $?"
  fi
}


###############################################################################
#                             File manipulation                               # 
###############################################################################

###############################################################################
# Searchs for an specific line of text in a given file. If it is not present
# appends it to the end of the file in a new line.
# Globals:
#   Exit status of last task - $?
# Arguments:
#   $1 -> Line of text
#   $2 -> File name
# Retuns:
#   None
###############################################################################
add_line_if_not_present_in_file(){
  line="${1}"
  file="${2}"
  # Use 'grep' to check if the line is in the file
  grep "$line" "$file" 
  # grep's exit status will tell if the line is present or nor
  if  [[ $? -ne 0 ]]; then
    echo "${line}" >> "${file}"
  fi
}

###############################################################################
#                                Variables                                    #
###############################################################################

###############################################################################
# Defines the proper "run commands" file to add cargo.
# Globals:
#   None
# Arguments:
#   None
# Retuns:
#   None
###############################################################################
get_rc_file(){
  current_shell=$(echo $SHELL)
  echo "${current_shell}"
  if    [[ "${current_shell}" == "/bin/bash" ]]; then
      RC_FILE="bashrc"
  elif  [[ "${current_shell}" == "/bin/zsh" ]]; then
      RC_FILE="zshrc"
  fi
}


###############################################################################
#                         Reusable configuration                              #
###############################################################################

###############################################################################
# Executes script abbreviations.sh with the proper flag
# Globals:
#   None
# Arguments:
#   Flag:
#     - z: to setup aliases for zsh
#     - b: to setup aliases for bash
# Retuns:
#   None
###############################################################################
execute_abbreviations_script(){
  green_text "Adding aliases/abbreviations..."
  ABBREVIATIONS_SCRIPT='set_ups/productivity/productivity_utils/abbreviations/abbreviations.sh'
  bash ${ABBREVIATIONS_SCRIPT} -"${1}"
  command_assertion
}
