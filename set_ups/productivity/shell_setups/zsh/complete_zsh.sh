#!/bin/bash

###############################################################################
#                              Zsh Completion                                 #
###############################################################################

###############################################################################
# Completes the productivity set up for zsh by adding aliases, missing env for
# .zshrc file and adding powerlevel10k
# This script file is running purely in zsh
###############################################################################

# Importing text library functions to format text and asseritions
source utils/lib.sh


###############################################################################
#                        RC file Specific configuration                       #
###############################################################################
# Cargo's path:
green_text "Updating RUST's cargo path to ~/.zshrc"
echo -ne "\n# Setup RUST environment setting in zsh - https://www.rust-lang.org/tools/install
source $HOME/.cargo/env
" >> ~/.zshrc
command_assertion

# Powerlevel10k - https://github.com/romkatv/powerlevel10k#installation
green_text "Installing Powerlevel10k..."
mkdir -p ~/Packages
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k ~/Packages/
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
command_assertion

# zsh-atosuggestions - https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
green_text "Installing zsh-suggestions..."
green_text "cloning the package..."
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions ~/Packages/
command_assertion
echo -ne "\n# zhs-suggestions - https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
" >> ~/.zshrc
command_assertion

# Vim key bindings for zsh
echo -ne "\n# Vim keybindings for zsh
bindkey -v"

#############################################################################ojkj#
#                                   Aliases                                   #
###############################################################################
# Calling to functions that execute's the aliases addition, with flag 'z' for zsh
execute_abbreviations_script z

echo "Please restart your terminal to finish Powelevel10k final setup"
echo "Also remember to visit your config: https://github.com/Rbatistab/my_configs"
