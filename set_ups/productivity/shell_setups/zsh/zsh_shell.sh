#!/bin/bash

###############################################################################
#                                 Zsh shell                                   #
###############################################################################

###############################################################################
# Zsh is an extension of bash shell, adding features
###############################################################################

# Importing text library functions to format text and asseritions
source utils/lib.sh

# Zsh shell - https://www.geeksforgeeks.org/how-to-install-z-shellzsh-on-linux/
green_text "Installing zsh..."
sudo apt-get update
yes | sudo apt-get install zsh
chsh -s /usr/bin/zsh
command_assertion

# Oh-my-zsh - https://ohmyz.sh/#install
green_text "Installing Oh-my-zsh..."
yes | sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
command_assertion
