#!/bin/bash

###############################################################################
#                           General productivity:                             #
###############################################################################

###############################################################################
# This is a space to add the software that helps your common productivity work-
# flow, thinks like todoists, emacs-orgmode, multiclipers, web serch shortcuts
# and many others specific to your personal preferences
###############################################################################

# Importing text library functions to format text and asseritions
source utils/lib.sh

# Notion - https://snapcraft.io/notion-snap
green_text "Installing Notion..."
sudo snap install notion-snap
command_assertion

# Ulauncher - https://ulauncher.io/
green_text "Installing Ulauncher..."
green_text "Adding repository..."
yes | sudo add-apt-repository ppa:agornostal/Ulauncher
command_assertion
yes | sudo apt update
yes | sudo apt install ulauncher
command_assertion

# CopyQ - https://github.com/hluk/CopyQ
green_text "Installing copyq..."
yes | sudo apt install copyq
command_assertion
green_text "Installing copyq-plugins..."
yes | sudo apt install copyq-plugins
command_assertion

# Powerline fonts - https://github.com/powerline/fonts
green_text "Installing Powerline fonts..."
sudo apt-get install fonts-powerline
command_assertion
