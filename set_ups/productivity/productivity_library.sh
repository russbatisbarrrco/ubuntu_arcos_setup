#!/bin/bash

###############################################################################
# This is a space to store the commands to install software you use in your com-
# puter. This is meant to be a library of software that you may or may not need
# on your next environment set up, but will come in handy to quickly install as
# basic utils, toolbox or specific utils when you are installing a new OS, or
# updating an existing one
###############################################################################


###############################################################################
#                         Productivity Library:                               #
###############################################################################

# Powerline fonts - https://github.com/powerline/fonts
green_text "Installing Powerline fonts..."
sudo apt-get install fonts-powerline
command_assertion
# Alternative installation:
#git clone https://github.com/powerline/fonts.git --depth=1
# install
#cd fonts
#./install.sh
# clean-up a bit
#cd ..
#rm -rf fonts

