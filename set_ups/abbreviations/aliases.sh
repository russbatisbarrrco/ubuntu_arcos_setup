#!/bin/bash

###############################################################################
# This file contains an associative array with aliases or abbreviations that 
# will be later used to create them according to the shell being used.
##############################################################################

declare -A aliases_list
aliases_list=(
  [armv]='sudo apt autoremove'
  [bckt]='git checkout -b '
  [cd.]='code .'
  [ckt]='git checkout '
  [cmt]='git commit -m'
  [gadd]='git add'
  [gupd]='sudo apt-get update'
  [gupg]='sudo apt-get upgrade'
  [linup]='sudo apt-get update && sudo apt-get upgrade -y'
	[nv]='nvim'
	[nv_config]='nvim ~/.config/nvim/init.vim'
  [p3]='python3'
  [pll]='git pull'
  [psh]='git push'
  [pyts]='cd ~/Documents/sandboxes/Python'
  [rsts]='cd ~/Documents/sandboxes/Rust'
  [sdbx]='cd ~/Documents/sandbox'
  [shtdwn]='sudo poweroff'
	[source_zsh]='source ~/.zshrc'
  [sts]='git status'
  [upd]='sudo apt update'
  [upg]='sudo apt upgrade'
	[vim_config]='vim ~/.vimrc'
  [xclp]='xclip -sel clip'
)
